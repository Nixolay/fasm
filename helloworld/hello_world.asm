; :set ft=fasm - select syntax
; ELF - linux, 64 - bit
format ELF64

; -- Registers bit:
; rax   - 64
; eax   - 32
; ax    - 16
; ah/al - 8

; -- function declaration
public _start

_start:
	call exit

exit:
	mov rax, 1 ; 1 - exit
	mov rbx, 0 ; 0 - return
	int 0x80

